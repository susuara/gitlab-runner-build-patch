#!/bin/bash

# Function to display error and exit
function error_exit {
    echo "Error: $1" >&2
    exit 1
}

# Get the absolute directory of the script
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# Define paths relative to the script location
PROJECT_FOLDER="$SCRIPT_DIR/.."
PATCH_FOLDER="$SCRIPT_DIR/patched-build-files"

# Check if patch folder exists
if [ ! -d "$PATCH_FOLDER" ]; then
    error_exit "Patch folder '$PATCH_FOLDER' not found."
fi

# Check if project folder exists
if [ ! -d "$PROJECT_FOLDER" ]; then
    error_exit "Project folder '$PROJECT_FOLDER' not found."
fi

echo "GitLab Runners Project Folder: $PROJECT_FOLDER"
echo "GitLab Runners Build Patch Folder: $PATCH_FOLDER"

# Function to patch files
function patch_build_files {
    local source_file="$1"
    local destination_file="$2"

    # Check if source file exists
    if [ ! -f "$source_file" ]; then
        error_exit "Source file '$source_file' not found."
    fi

    # Check if destination directory exists
    local destination_dir=$(dirname "$destination_file")
    if [ ! -d "$destination_dir" ]; then
        error_exit "Destination directory '$destination_dir' not found."
    fi

    # Patch the file
    cp "$source_file" "$destination_file" || error_exit "Failed to patch '$destination_file'."
    echo "Patched: $destination_file"
}

# Patch files
patch_build_files "$PATCH_FOLDER/release.gitlab-ci.yml" "$PROJECT_FOLDER/.gitlab/ci/release.gitlab-ci.yml"
patch_build_files "$PATCH_FOLDER/Makefile" "$PROJECT_FOLDER/Makefile"
patch_build_files "$PATCH_FOLDER/Makefile.build.mk" "$PROJECT_FOLDER/Makefile.build.mk"
patch_build_files "$PATCH_FOLDER/Makefile.runner_helper.mk" "$PROJECT_FOLDER/Makefile.runner_helper.mk"
patch_build_files "$PATCH_FOLDER/variables.go" "$PROJECT_FOLDER/magefiles/ci/variables.go"
patch_build_files "$PATCH_FOLDER/images.go" "$PROJECT_FOLDER/magefiles/images.go"
patch_build_files "$PATCH_FOLDER/helper.go" "$PROJECT_FOLDER/magefiles/images/helper.go"
patch_build_files "$PATCH_FOLDER/package.go" "$PROJECT_FOLDER/magefiles/package.go"
patch_build_files "$PATCH_FOLDER/blueprint.go" "$PROJECT_FOLDER/magefiles/packages/blueprint.go"
patch_build_files "$PATCH_FOLDER/create.go" "$PROJECT_FOLDER/magefiles/packages/create.go"
patch_build_files "$PATCH_FOLDER/buildx.go" "$PROJECT_FOLDER/magefiles/docker/buildx.go"

echo "All Build Patches Applied Successfully."
